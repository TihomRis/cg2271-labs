/*
 * lab4part1.cpp
 *
 * Created: 2/15/2014 11:08:41 AM
 *  Author: MohitSridhar
 */ 


#include <avr/io.h>
#include <Arduino.h>
#include "kernel.h"

#define NUMTASKS 2

void task1 (void *arg)
{
	while(1)
	{
		digitalWrite(6, HIGH);
		OSSleep(100);
		digitalWrite(6, LOW);
		OSSleep(100);		
	}
}

void task2 (void *arg)
{
	while(1)
	{
		digitalWrite(7, HIGH);
		OSSleep(500);
		digitalWrite(7, LOW);
		OSSleep(500);
	}
}

void setup()
{
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
	
	OSInit(NUMTASKS);
	
	OSCreateTask(0, task1, NULL);
	OSCreateTask(1, task2, NULL);
	
	OSRun();
	
}

void loop()
{
	// Empty
}

int main(void)
{
	init();
	setup();
    while(1)
    {
        loop();
		if(serialEventRun)
			serialEventRun();
    }
}