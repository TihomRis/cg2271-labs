/*
 * lab2part2.cpp
 * Author: Mohit Shridhar
 */ 


#include <avr/io.h> 
#include <Arduino.h>

#define button1 2
#define button2 3

#define analogOut 6
#define digitalOut 7 
#define analogChannel 0

long prevMillis = 0;
int ledState = LOW;

void setup()
{
	pinMode(button1, INPUT);  
	pinMode(button2, INPUT);
	
	pinMode(analogOut, OUTPUT); 
	pinMode(digitalOut, OUTPUT);
	Serial.begin(9600);
	
}

/**
	Note: The following flashPin function doesn't use the 'delay' function. While using the delay function
	either "blinking" or "brightness adjustment" can be executed with the required priority. Simultaneously controlling 
	both is not possible since Arduino doesn't have a multi-core processor. So the work around is to use the inbuilt timer
	function to process delays and continue looping through the rest of the program instead of suspending the entire
	program while processing the flashing part like in delay function. 
*/

void flashPin(int pin, int delayVal)
{
	long currMillis = millis();
	
	if(currMillis - prevMillis > delayVal) {
		// save the last time you blinked the LED
		prevMillis = currMillis;

		// if the LED is off turn it on and vice-versa:
		if (ledState == LOW)
			ledState = HIGH;
		else
			ledState = LOW;

		// set the LED with the ledState of the variable:
		digitalWrite(pin, ledState);
	}
}

/************************************************************************/
/* This function remaps 10 bit values into 8 bit values                 */
/************************************************************************/

int remap1023to255(int val) 
{	
	double percentage = val / 1023.0;
	return percentage * 255.0; // Auto-unboxing: INT conversion
}

/************************************************************************/
/* This function remap any given range into 8 bit values                */
/************************************************************************/

int remapto255 (int val, int inLow, int inHigh) 
{
	double percentage = (val * 1.0) / (inHigh - inLow);
	return (percentage * 255.0) + inLow; // Auto-unboxing: INT conversion
}

void processPot() {
	
	int val=analogRead(A0);
	val = remap1023to255(val);
		
	analogWrite(analogOut, val);
		
	Serial.print(val);
	Serial.print(" ");
	
}

int processTouch(int touch)
{
	touch = analogRead(A1);
	touch = remapto255(touch, 0, 1023);
			
	Serial.print(touch);
	Serial.println();
				
	flashPin(digitalOut, touch);
	
	return touch;
}


int loop(int touch) {
	
	if (digitalRead(button1) == HIGH) {
		processPot();
	}
	 
	if (digitalRead(button2) == HIGH) {
		touch = processTouch(touch);
	}
	else {
		Serial.println();
	}
	
	// Keep flashing the pin with previous value if button2 is not pressed
	flashPin(digitalOut, touch);
	
	return touch;
}

int main(void) {
	init();
	setup();
	
	int touch = 0;
	while(1)
	{	
		touch = loop(touch); 
		if(serialEventRun)
			serialEventRun();
	}
		return 0; 
}
