/*
 * lab4part3.cpp
 *
 * Created: 2/15/2014 11:51:08 AM
 *  Author: MohitSridhar
 */ 


#include <avr/io.h>
#include <Arduino.h>
#include "kernel.h"
#include "sema.h"

#define NUMTASKS 2

// Do switch debouncing
unsigned long int0time=0, int1time=0;
OSSema task1Go, task2Go;



/**
 Debouncing function. Returns TRUE if this interrupt was not caused by a bouncing switch
 In plain English: If the switch was faulty output FALSE to indicate false signal
*/

int debounce(unsigned long *debTimer)
{
	unsigned long tmp=*debTimer;
	unsigned long currTime=OSticks();
	
	if((currTime-tmp) > 500)
	{
		*debTimer=currTime;
		return 1;
	}
	else
	return 0;
	
}

void task1 (void *param)
{
	while (1) {
	
		OSTakeSema(&task1Go);	//If task1Go is non-zero (when the INT0 button is pressed): 
								//runs the LED flash code below. Else, this task will be blocked
	
		for (int i=0; i<5; i++)
		{
			digitalWrite(6, HIGH);
			OSSleep(250);
			digitalWrite(6, LOW);
			OSSleep(250);
		}
	
	}
		
	
}

void task2 (void *param)
{
	
	while (1) {
	
		OSTakeSema(&task2Go);	//If task2Go is non-zero (when the INT1 button is pressed):
								//runs the LED flash code below. Else, this task will be blocked
	
		for (int i=0; i<5; i++)
		{
			digitalWrite(7, HIGH);
			OSSleep(250);
			digitalWrite(7, LOW);
			OSSleep(250);
		}
	}

}

void int0ISR()
{
	if (debounce(&int0time) == false)
		return; // if false signal
	
	OSGiveSema(&task1Go); // Set task1Go as '1'
}

void int1ISR()
{
	if (debounce(&int1time) == false)
		return; // if false signal
	
	OSGiveSema(&task2Go); // Set task2Go as '1'
}

void setup()
{
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
		
	OSInit(NUMTASKS);
	
	attachInterrupt(0, int0ISR, RISING);
	attachInterrupt(1, int1ISR, RISING);
	
	// Initialize semaphores
	OSCreateSema(&task1Go, 0, 1); //task1Go set as '0'
	OSCreateSema(&task2Go, 0, 1); //task2Go set as '0'
		
	OSCreateTask(0, task1, NULL); //Note Task1 has higher priority here.
	OSCreateTask(1, task2, NULL);
		
	OSRun();
}

void loop()
{
	// Empty
}

// Do not modify
int main()
{
	init();
	setup();
	
	while(1)
	{
		loop();
		
		if(serialEventRun)
		serialEventRun();
	}
}