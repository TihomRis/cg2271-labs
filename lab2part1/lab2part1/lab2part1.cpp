/*
* Lab2Part1.cpp
*
* Created: 28/8/2013 2:39:27 PM
* Author: You
*/
#include <avr/io.h>
#include <Arduino.h>
#define polledPin 2
#define analogOut 6
#define analogChannel 0
void setup()
{
	pinMode(2, INPUT);
	pinMode(7, OUTPUT);
	Serial.begin(9600);
}
void flashPin7(int delayVal)
{
	digitalWrite(7, HIGH);
	delay(delayVal);
	digitalWrite(7, LOW);
	delay(delayVal);
}

int remapTouch(int val) {
	return (val/894.0)*375.0+125;
}

int remap (int val){
	return (val/1023.0)*255.0;
}

void loop()
{
	int val=analogRead(0);
	val= remap(val);
	int touch=analogRead(1);
	touch= remapTouch(touch);
	Serial.print(val);
	Serial.print(" ");
	Serial.print(touch);
	Serial.println();
	analogWrite(6, val);
	flashPin7(touch);
	delay(500);
}


// Note: Do not modify main. It has been written to work correctly with
// the Arduino library. Modify only setup() and loop(), though you may
// add new functions.
int main(void)
{
	init();
	setup();
	while(1)
	{
		loop();
		if(serialEventRun)
		serialEventRun();
	}
	return 0;
}