#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

#define NUMELTS 16384

int prime (int n)
{
	int ret = 1, i;

	for (i=2; i<=(int) sqrt(n) && ret; i++) {
		ret = n % i;
	}

	return ret;
}

int main()
{
	int data[NUMELTS];

	//Pipe variables:
	int fd[2];
	int childPrimeCount = 0, parentPrimeCount = 0;
	int childReturnCount, status;
	pipe(fd);

	srand(time(NULL));

	for (int i=0; i<NUMELTS; i++) {
		data[i] = (int) (((double) rand() / (double) RAND_MAX) * 10000);
	}

	// PARENT:
	if (fork()) { 
		for (int j=0; j<8192; j++) {
			if (prime(data[j]) == 1) {
				parentPrimeCount++;
			}
		}

		printf("Address of first rand num in PARENT: %d\n", &data[0]);
		printf("Content in child %d\n", data[0]);
		// Wait for child to finish
		wait(&status);

		// Read data from child:
		close(fd[1]);
		read(fd[0], &childReturnCount, sizeof(childReturnCount));

		printf("Parent: %d + Child: %d = %d total primes\n", parentPrimeCount, childReturnCount, parentPrimeCount+childReturnCount);

	// CHILD:
	} else {
		for (int k=8192; k < NUMELTS; k++) {
			if (prime(data[k]) == 1) {
				childPrimeCount++;
			}
		}

		printf("Address of first rand num in CHILD: %d\n", &data[0]);
		printf("Content in child %d\n", data[0]);
		// Write to parent the prime number count:
		close(fd[0]);
		write(fd[1], &childPrimeCount, sizeof(childPrimeCount));

	}


}