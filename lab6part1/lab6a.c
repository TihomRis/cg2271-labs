#include <stdio.h>
#include <unistd.h>

#define PARENT_DELAY 0 // Change to 1
#define CHILD_DELAY 0 // Change to 2

int main()
{
	pid_t pid;
	int i, j, k, status;

	k = 15;

	if (pid = fork())
	{
		printf("Process ID of child: %d\n", pid);

		for (int i=0; i<10; i++) {
			printf("Parent: i=%d k=%d\n", i, k);
			printf("Address of K in parent: %lx\n", &k);
			k++;

			sleep(PARENT_DELAY);
		}
		
	} else {
		for (i=10; i<50; i++) {
			printf("Child: i=%d, k=%d\n", i, k);
			printf("Address of K in child: %lx\n", &k);
			sleep(CHILD_DELAY);
		}
	}

}


/* OUTPUT:

Finding Parent ID - 
1. Run ./lab6a
2. ps -p <pid> -o ppid

*/