#include <stdio.h>
#include <unistd.h>

#define PARENT_DELAY 1 
#define CHILD_DELAY 2

int main()
{
	pid_t pid;
	int i, j, k, status;

	k = 15;

	if (pid = fork())
	{
		printf("Process ID of child: %d\n", pid);

		for (int i=0; i<10; i++) {
			printf("Parent: i=%d k=%d\n", i, k);
			k++;

			sleep(PARENT_DELAY);
		}

		wait(&status);

	} else {
		for (i=10; i<50; i++) {
			printf("Child: i=%d, k=%d\n", i, k);
			sleep(CHILD_DELAY);
		}
	}

}
