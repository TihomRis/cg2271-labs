/*
 * cg2271lab3part2.cpp
 *
 * Created: 14/9/2013
 *  Author: Mohit Shridhar
 */ 


#include <avr/io.h>
#include <Arduino.h>
#include "prioq.h"

#define INPUT1INT 0
#define INPUT2INT 1

#define INPUT1 A0
#define INPUT2 A1

#define OUT1 6
#define OUT2 7

unsigned int data1;
unsigned int data2;

TPrioQueue *queue;

// Do switch debouncing
unsigned long int0time=0, int1time=0; //last debounce times

/**
 Debouncing function. Returns TRUE if this interrupt was not caused by a bouncing switch
 In plain English: If the switch was faulty output FALSE to indicate false signal
*/

int debounce(unsigned long *debTimer)
{
	unsigned long tmp=*debTimer;
	unsigned long currTime=millis();
	
	if((currTime-tmp) > 500) // Check if the switch reading was consistent for at least 500 ms
	{
		*debTimer=currTime; // Update last debounce time
		return 1;
	}	
	else
		return 0;
		
}

// Declares a new type called "funcptr". To be later used for typecasting

typedef void (*funcptr)(void);

// Flashes LED at pin 7 5 times a 4 Hz
void int0task()
{
	int ctr=0;
	for(int i=0; i<5; i++)
	{
		digitalWrite(7, HIGH);
		delay(125);
		digitalWrite(7, LOW);
		delay(125);
		Serial.println(ctr);
		ctr++;
	}
}

// Flashes LED at pin 6 5 times at 2HZ
void int1task()
{
	for(int i=0; i<5; i++) // Correction: changed end limit from 1 to 5
	{
		digitalWrite(6, HIGH);
		delay(250);
		digitalWrite(6, LOW);
		delay(250);
	}
}

void int0ISR()
{
	if (debounce(&int0time) == false)
		return; // false signal
		
	enq(queue, (void *) int0task, 0); // enqueue INT0task with the highest priority. Not the func itself, but a pointer to task func.
	Serial.println("Enqueued 0");
}

void int1ISR()
{
	if (debounce(&int1time) == false)
		return; // false signal
	
	enq(queue, (void *) int1task, 1); // enqueue INT1task with 2nd highest priority
	Serial.println("Enqueued 1");
}

void setup()
{
	queue=makeQueue();
	
	attachInterrupt(INPUT1INT, int0ISR, RISING);
	attachInterrupt(INPUT2INT, int1ISR, RISING);
		
	pinMode(OUT1, OUTPUT);
	pinMode(OUT2, OUTPUT);
	Serial.begin(9600);	
}

// Dequeues and calls functions if the queue is not empty
 
void loop()
{
	Serial.print("Number of items in queue: ");
	Serial.println(qlen(queue));
	
	if (qlen(queue) > 0)
	{
		funcptr f = (funcptr) deq(queue); // Get the pointer to the function to be executed next
		f();					  // Execute it
	}
	
	delay(1500);				  // So that we have enough time to witness the flashing LEDS 
}

int main(void)
{
	init();
	setup();
    while(1)
    {
		loop();
		
		if(serialEventRun)
			serialEventRun();
    }
}