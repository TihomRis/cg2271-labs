#include <stdio.h>
#include <pthread.h>

int ctr=0;
pthread_t thread[10];

void *child(void *t)
{
	printf("I am child %d. Ctr=%d\n", t, ctr);

	ctr++;

	printf("After completing main(): I am child %d. Ctr=%d\n", t, ctr);

	pthread_exit(NULL);
}

int main()
{
	int i;

	ctr = 0;

	for (i=0; i<10; i++) {

		if (i > 0) { // If multiple threads are running
			pthread_join(thread[i-1], NULL); // Wait for i-1 thread to finish
		}

		pthread_create(&thread[i], NULL, child, (void *) i);
	}


	pthread_join(thread[9], NULL);

	printf("Value of ctr=%d\n", ctr);
	return 0;
}