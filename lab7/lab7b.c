#include <stdio.h>
#include <pthread.h>

// pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex;

int glob;
pthread_t thread[10];

void *child(void *t) 
{
	printf("Child %d entering. Glob is currently %d\n", t, glob);

	pthread_mutex_lock(&mutex);
	printf("Mutex was locked\n");
	glob++;
	sleep(1);
	glob++;
	pthread_mutex_unlock(&mutex);
	printf("Mutex was unlocked\n");
	printf("Child %d exiting, Glob is currently %d\n", t, glob);

	pthread_exit(NULL);
}

int main()
{
	int i, quit = 0;
	glob = 0;

	pthread_mutex_init ( &mutex, NULL);

	for (int i=0; i<10; i++) {
		/* Using pthreads: */
		pthread_create(&thread[i], NULL, child, (void *) i);
	}

	/* Wait for glob to reach 20 or pthread_join(thread[9], NULL) can be used */
	while (glob<20)
		wait(1);

	int mutexExe = pthread_mutex_destroy(&mutex);
	

	printf("Final value of glob is %d\n mutex return value %d\n", glob, mutexExe);


	
	return 0;
}