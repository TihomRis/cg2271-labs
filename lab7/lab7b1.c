#include <stdio.h>
#include <pthread.h>

int glob;

void *child(void *t) 
{
	printf("Child %d entering. Glob is currently %d\n", t, glob);

	glob++;
	sleep(1);
	glob++;

	printf("Child %d exiting, Glob is currently %d\n", t, glob);
}

int main()
{
	int i;
	glob = 0;

	for (int i=0; i<10; i++) {
		
		/* Normal function call Q6 */
		child((void *) i);
	}

	printf("Final value of glob is %d\n", glob);
	
	return 0;
}