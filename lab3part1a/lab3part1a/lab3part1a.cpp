/*
 * lab3part1a.cpp
 *
 * Created: 2/8/2014
 *  Author: MohitSridhar
 */ 


#include <avr/io.h>
#include <Arduino.h>
#include "prioq.h"

#define QLEN 10

TPrioQueue *queue;

void setup()
{
	queue=makeQueue();
	
	Serial.begin(9600);
	
	for (int i=0; i<QLEN; i++)  {
		enq(queue, (void *) i, QLEN-i-1);
		
	/** Question 2:
	
		Serial.print("Enqueue: ");
		Serial.println(i);
		delay(500);
	
	*/
	}
}

void loop()
{
	int val;
	
	if (qlen(queue) > 0)
	{
		val = (int) deq(queue);
		Serial.println(val);
	}
	
	delay(500);
	
}

int main(void)
{	
	init();
	setup();
	
    while(1)
    {
        loop();
		if (serialEventRun)
			serialEventRun(); 
    }
}