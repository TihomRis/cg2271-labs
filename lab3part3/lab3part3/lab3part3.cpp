/*
 * lab3part3.cpp
 *
 * Created: 2/8/2014 
 *  Author: MohitSridhar
 */ 


#include <Arduino.h>

void togglePin6()
{
	static char state=1;
	
	if(state)
	digitalWrite(6, HIGH);
	else
	digitalWrite(6, LOW);
	
	state=!state;
}


void togglePin7()
{
	static char state=1;
	
	if(state)
	digitalWrite(7, HIGH);
	else
	digitalWrite(7, LOW);
	
	state=!state;
}

void setup()
{
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
}

void fastLoop()
{
	togglePin6();
}

unsigned int slowLoopCounter = 0;

void slowLoop()
{
	slowLoopCounter++; // Accumulate five 100 ms cycles to initiate slow LED toggle
	if (slowLoopCounter >= 5) { 
		togglePin7();
		slowLoopCounter = 0;
	}
}

unsigned long fast_loopTimer = 0;

void loop()
{
	while (1) 
	{
		if (millis() - fast_loopTimer > 99) { // Toggle LED (Pin 6) at a rate of 10 Hz (100 ms) ==> 5 On/Off cycles 
			
			fastLoop(); // Execute every 100 ms
			slowLoop(); // Execute every 500 ms
			
			fast_loopTimer = millis();
		}
	}
}

int main()
{
	init();
	setup();
	
	while(1)
	{
		loop();
		if(serialEventRun)
		serialEventRun();
	}
}