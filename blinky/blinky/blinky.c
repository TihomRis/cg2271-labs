

/*
* blinky.c
*
* Created: 24/8/2012 4:56:47 PM
* Author: dcstanc
*/
#include <avr/io.h> // Header file to access Atmega328 I/O registers
#include <Arduino.h> // Header file for the Arduino library
#define GREEN_PIN 12
#define RED_PIN 13
void blink_led(unsigned pinnum)
{
	digitalWrite(pinnum, HIGH); // Set digital I/O pin to a 1
	//delay(1000); // Delay
	digitalWrite(pinnum, LOW); // Set digital I/O pin to a 0
	//delay(1000); // Delay
}
void setup()
{
	pinMode(GREEN_PIN, OUTPUT); // Set digital I/O pins 12
	pinMode(RED_PIN, OUTPUT); // and 13 to OUTPUT.13
}

void update()
{
	static int x=0;
	x=x+1;
}

int main(void)
{
	init(); // Initialize arduino. Important!
	setup(); // Set up the pins
	while(1)
	{
		
		while(1)
		{
			blink_led(RED_PIN); // existing code
			update(); // new line
			blink_led(GREEN_PIN); // existing code
		}
		
	}
}