/*
 * lab4part2.cpp
 *
 * Created: 2/15/2014 11:33:18 AM
 *  Author: MohitSridhar
 */ 


#include <avr/io.h>
#include <Arduino.h>
#include "kernel.h"
#include "sema.h"

#define NUMTASKS 2

OSSema task1Go, task2Go;

void task1(void *param)
{
	while(1) 
	{
		OSTakeSema(&task1Go);
		for (int i=0; i<5; i++) 
		{
			digitalWrite(6, HIGH);
			OSSleep(250);
			digitalWrite(6, LOW);
			OSSleep(250);	
		}	
		
		OSGiveSema(&task2Go);
	}
	
}

void task2(void *param)
{
	
	while(1)
	{
		OSTakeSema(&task2Go);
		for (int i=0; i<2; i++) 
		{
			digitalWrite(7, HIGH);
			OSSleep(500);
			digitalWrite(7, LOW);
			OSSleep(500);
		}
		
		OSGiveSema(&task1Go);
		
	}
	
	
}

void setup()
{
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
	
	OSInit(NUMTASKS);
	
	OSCreateSema(&task1Go, 1, 1);
	OSCreateSema(&task2Go, 0, 1);
	
	OSCreateTask(0, task1, NULL);
	OSCreateTask(1, task2, NULL);
	
	OSRun();
}

void loop()
{
	
}

int main(void)
{
    init();
	setup();
	
	while(1)
    {
        loop();
		if (serialEventRun)
			serialEventRun(); 
    }
}