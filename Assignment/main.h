#include <stdio.h>
#include <math.h>
#include <time.h>
#include "memmang.h"
#include "timing.h"

void testAlloc(void **ptr, unsigned long allocLen);
void testFree(void *ptr);
void showMemStats();
double variance(double mean);
void fillMemory();