//
//  main.c
//  memmang
//
//  Created by Colin Tan on 22/10/13.
//  Copyright (c) 2013 ceg. All rights reserved.
//

#include <stdio.h>
#include <math.h>
#include <time.h>
#include "memmang.h"
#include "timing.h"
#include "nano_timer.h"

#include <unistd.h>

#include <sys/time.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

/* ----------------------------------------------
 
 Memory test routines. Do not modify this file
 ----------------------------------------------- */

unsigned long totalTimeSpent=0;
unsigned long freeTimeSpent=0;
unsigned long recordedTime[MAXSAMPLES];
unsigned long cumInternalFragmentation=0;
unsigned long cum=0;
void *ptr[20];

void testAlloc(void **ptr, unsigned long allocLen)
{
    struct timespec _nsStart, _nsEnd;
    unsigned long frag, allocTime;

    getNanoSeconds(&_nsStart);
    *ptr=NSAlloc(allocLen);

    if(*ptr!=NULL)
    {
        getNanoSeconds(&_nsEnd);
        allocTime = _nsEnd.tv_nsec - _nsStart.tv_nsec;
        recordedTime[cum] = allocTime;

        cum++;
        frag=(int) fabs((double)(NSGetLastAllocLen() - allocLen));
        
        cumInternalFragmentation+=frag;

        printf("Address: %ld Size Requested: %lu Size Allocated: %lu Internal Frag: %lu\n\n", (unsigned long)*ptr, allocLen, NSGetLastAllocLen(), frag);
    }
    else {
        printf("Allocation failed. Out of memory?\n");
    }
}

void testFree(void *ptr)
{
    
    if(ptr==NULL)
        return;
    
    NSFree(ptr);
}

void showMemStats()
{
    double totalFree, largestBlock, frag;
    
    computeMemoryStats(&totalFree, &largestBlock, &frag);
    
    printf("\nTotal Free: %4.2f Largest Block: %4.2f Fragmentation: %4.2f\n\n", totalFree, largestBlock, frag);
    
    
}
double variance(double mean)
{
    double sum=0.0;
    
    #if EXP_TYPE == RUNTIME

    for(int i=0; i<cum; i++) {
        if ((recordedTime[i]/1000.0) < 10000000000000.0) {
            sum+=(((recordedTime[i]/1000.0)-mean) * ((recordedTime[i]/1000.0)-mean));
        }
        
    }


    #elif EXP_TYPE == ALLOCFREE

    for(int i=0; i<cum; i++) {
        if ((recordedTime[i]) < 10000000000000.0) {
            sum+=(((recordedTime[i])-mean) * ((recordedTime[i])-mean));
        }
        
    }

    #endif

    
    return sum/(cum-1.0);
}

void fillMemory()
{
    // Fill up memory completely
    testAlloc(&ptr[0], 1);
    testAlloc(&ptr[1], 2);
    testAlloc(&ptr[2], 4);
    testAlloc(&ptr[3], 8);
    testAlloc(&ptr[4], 16);
    testAlloc(&ptr[5], 32);
    testAlloc(&ptr[6], 64);
    testAlloc(&ptr[7], 128);
    testAlloc(&ptr[8], 256);
}

int main(int argc, const char * argv[])
{
    
    
    initializeMemory();
    initNanoClock();
    
    
#if EXP_TYPE == RUNTIME
    int i;
    
    // Finding running times
    //clock_t allocBegin, allocEnd, freeBegin, freeEnd;

    struct timespec nsAllocBegin, nsAllocEnd, nsFreeBegin, nsFreeEnd;

    unsigned long cumFree=0, cumAlloc=0;
    
    for(i=0; i<MAXRUNS; i++)
    {   

        // Fill memory
        fillMemory();
        
        //startClock(&freeBegin);
        getNanoSeconds(&nsFreeBegin);


        // Start making holes
        testFree(ptr[8]);
        testFree(ptr[2]);
        testFree(ptr[5]);

        //endClock(&freeEnd, &freeBegin);
        getNanoSeconds(&nsFreeEnd);

        //cumFree+=(freeEnd - freeBegin);
        cumFree += (nsFreeEnd.tv_nsec - nsFreeBegin.tv_nsec);

        // startClock(&allocBegin);
        getNanoSeconds(&nsAllocBegin);

        // Allocate new items
        testAlloc(&ptr[2], 4);
        testAlloc(&ptr[5], 16);
        testAlloc(&ptr[8], 128);
        
        //endClock(&allocEnd, &allocBegin);
        getNanoSeconds(&nsAllocEnd);
        
        //cumAlloc+=(allocEnd-allocBegin);
        cumAlloc += (nsAllocEnd.tv_nsec - nsFreeBegin.tv_nsec);

        //startClock(&freeBegin);
        getNanoSeconds(&nsFreeBegin);

        // Free everything
        for(int i=0; i<9; i++)
            testFree(ptr[i]);

        //startClock(&freeEnd);
        getNanoSeconds(&nsFreeEnd);
    }
#else
    // Find statistics to compare memory allocation schemes
    
    // Fill the entire memory

    unsigned long cumAlloc = 0;
    clock_t begin, end;

    startClock(&begin);
    fillMemory();
    
    // Now start creating holes and filling them
    testFree(ptr[4]);
    testFree(ptr[6]);
    
    testAlloc(&ptr[4], 4);
    testAlloc(&ptr[6], 48);
    testAlloc(&ptr[9], 4);
    testAlloc(&ptr[10], 9);
    
    testFree(ptr[3]);
    testAlloc(&ptr[3], 4);
    endClock(&end, &begin);
    cumAlloc+=(end-begin);

#endif

    #if EXP_TYPE == RUNTIME
    
    double mean=(double) (cumAlloc / 1000.0) / cum; // Microseconds
    
    #elif EXP_TYPE == ALLOCFREE

    double mean=(double) (cumAlloc) / cum; // milliseconds

    #endif

    printf("Average Time: %4.8f\n", mean);
    printf("Variance: %4.8f\n", variance(mean));
    printf("Total internal fragmentation: %6.2f kilobytes\n", cumInternalFragmentation/1024.0);
    printf("Average internal fragmentation: %4.2f bytes\n", (double) cumInternalFragmentation/cum);
    showMemStats();
    
    return 0;
}

