//
//  timing.h
//  memmang
//
//  Created by Colin Tan on 25/10/13.
//  Copyright (c) 2013 ceg. All rights reserved.
//

#ifndef memmang_timing_h
#define memmang_timing_h

void startClock(clock_t *_beginTime);
unsigned long endClock(clock_t *endTime, clock_t *beginTime);
unsigned long getLastTimeSpent();
#endif
