//
//  timing.c
//  memmang
//
//  Created by Colin Tan on 25/10/13.
//  Copyright (c) 2013 ceg. All rights reserved.
//

#include <stdio.h>
#include <time.h>
#include <sys/time.h>

unsigned long _timeSpent;

void startClock(clock_t *_beginTime)
{
        *_beginTime=clock();
}

unsigned long endClock(clock_t *_endTime, clock_t *_beginTime)
{
    *_endTime=clock();
    _timeSpent=*_endTime - *_beginTime;
    return *_endTime;
}

unsigned long getLastTimeSpent()
{
    return _timeSpent;
}