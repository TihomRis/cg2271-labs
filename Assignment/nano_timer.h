#include <time.h>
#include <sys/time.h>
#include <stdio.h>
 
#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

void getNanoSeconds(struct timespec *ts);
void initNanoClock(void);