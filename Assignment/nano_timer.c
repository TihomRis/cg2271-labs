/* 
os x, compile with: gcc -o testo test.c 
linux, compile with: gcc -o testo test.c -lrt
*/
 
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
 
#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif
 

clock_serv_t cclock;

void getNanoSeconds(struct timespec *ts) {
 
  #ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time

    
    mach_timespec_t mts;

    // host_get_clock_service(mach_host_self(), REALTIME_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    // mach_port_deallocate(mach_task_self(), cclock);
    
    ts->tv_sec = mts.tv_sec;
    ts->tv_nsec = mts.tv_nsec;


  #else
  
    clock_gettime(CLOCK_REALTIME, ts);
  
  #endif
 
}

void initNanoClock(void) {
    host_get_clock_service(mach_host_self(), REALTIME_CLOCK, &cclock);
    mach_port_deallocate(mach_task_self(), cclock);
}

 
// int main(int argc, char **argv) {
 
//   struct timespec ts;
//   getNanoSeconds(&ts);
 
//   printf("s:  %lu\n", ts.tv_sec);
//   printf("ns: %lu\n", ts.tv_nsec);
//   return 0;
 
// }