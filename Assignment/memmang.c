/*

CG2271 Lab Group 2: Term Assignment

Narin
Nigel
Mohit (A0105912N)

*/


#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include "memmang.h"
#include "linkedList.h"
#include "nodes.h"

/* -----------------------------------------------------------------
 
 Memory Management Utilities
 ---------------------------
 
 These are provided to help you build your memory manager. You do not
 need to modify the codes given here.
 
 ----------------------------------------------------------------- */

extern TMemoryNode *_head;

/* -----------------------------------------------------------------
 
 Memory Allocation Routines
 --------------------------
 
 These are the actual routines you will implement. You should modify only
 findFreeMemory, NSAlloc and NSFree.
 
 ----------------------------------------------------------------- */

unsigned long totalFoundBytes = 0;
unsigned long totalNotRequiredBytes = 0;

// The actual memory to allocate
char _memoryBlock[MEMMANG_MAXSIZE];
unsigned long _allocated_length;

void initializeMemory()
{

    // DO NOT MODIFY THIS CODE
    
    // Initialize the list of nodes
    createList(256);
    
    // Initializes the memory manager with a single node consisting of free memory
    // the size of memmang_maxsize, starting at location zero.
    TMemoryNode *myNewNode=newNode(0, MEMMANG_MAXSIZE, 0);
    myNewNode->startAddress=MEMMANG_BASEADDR;
    insertNode(myNewNode);
}

unsigned long NSGetLastAllocLen()
{
    return _allocated_length;
}


/* -------------------------------------------
 
    Modify only code below this line 
 
    ------------------------------------------- */

// Pre: requestedLen = # of bytes of memory requested
// Post: Returns pointer to memory management node (TMemoryNode) describing the available memory

TMemoryNode *findFreeMemory(unsigned long requestedLen)
{

    /* TODO: Implement algorithm to locate next free memory block
             using the allocation policy indicated */
    
    

    // ENSURE THAT YOUR CODE IS PROPERLY DOCUMENTED!!
    
    TMemoryNode *ptr = _head;

#if MEMMANG_TYPE==MEMMANG_BESTFIT

    TMemoryNode *bestFit = NULL; 

    while (ptr != NULL) {
        
        /* Conditions for Best Fit:
            1. Memory is not allocated
            2. Memory length is greater than or equal to the required length
            3. Shortest memory length that satisifies 1 & 2
         */  

        if (!ptr->allocated && (ptr->len >= requestedLen) && (bestFit == NULL || ptr->len < bestFit->len)) {
            bestFit = ptr;

            if (bestFit->len == requestedLen) {
                break; // If perfect match, no need to continue
            }
        }

        ptr = ptr->next;
    }

    return bestFit;
    
#elif MEMMANG_TYPE==MEMMANG_WORSTFIT

    TMemoryNode *worstFit = NULL;



    while (ptr != NULL) {

        /* Conditions for Worst Fit:
            1. Memory is not allocated
            2. Memory length is greater than or equal to the required length
            3. Longest memory length that satisfies 1 & 2
        */

        if (!ptr->allocated && (ptr->len >= requestedLen) && (worstFit == NULL || ptr->len > worstFit->len)) {
            worstFit = ptr;
        }

        ptr = ptr->next;
    }

    return worstFit;
    
#elif MEMMANG_TYPE==MEMMANG_FIRSTFIT
    
    // Search for free memory using firstFit fit policy
    TMemoryNode *firstFit = NULL;

    while (ptr != NULL) {

        /* Conditions for First Fit:
            1. Memory is not allocated
            2. Memory length is greater than or equal to the required length
        */

        if (!ptr->allocated && ptr->len >= requestedLen) {
            firstFit = ptr;
            break; 
        }

        ptr = ptr->next;
    }

    return firstFit;
    
#endif

    // dummy return to suppress compiler errors. Modify to return a pointer
    // to the TMemoryNode memory management node describing the free memory found.
    return NULL;
    
}

// Pre: requestedLen = Amount of memory requested in bytes
// Post: Memory is allocated and address of the start of the memory segment allocated is
//       returned. Return a NULL if there is no more memory to allocate.

void *NSAlloc(unsigned long requestedLen)
{
    // TODO: Implement memory allocation algorithm here, returning address of the
    // memory block allocated, cast as (void *).
    
    
    /*
        Implement the memory allocation algorithms as shown in the lecture notes.
     
        Hint: You can write a single allocation routine for all 3 strategies. The
                strategies differ only in the way that findFreeMemory is implemented.
     
     */

    // IMPORTANT: Set the global variable _allocated_length to the number of bytes
    //            of memory you are allocating in this request.
    
    // Dummy return statement to suppress compiler errors
    
    // ENSURE THAT YOUR CODE IS PROPERLY DOCUMENTED!!

    TMemoryNode* ptr = findFreeMemory(requestedLen);

    /* If no memory is available */
    if (ptr == NULL) {
        printf("No free memory available\n");
        _allocated_length = 0;
        return NULL; 
    }
    
    // printf("Requested: %lu bytes, Found: %lu bytes, Not required: %lu bytes\n", requestedLen, ptr->len, (ptr->len)-requestedLen);

    // totalFoundBytes += ptr->len;
    // totalNotRequiredBytes += (ptr->len)-requestedLen;

    // printf("Total Found: %lu Total Not Required: %lu, Failed request percentage: %f %%\n", totalFoundBytes, totalNotRequiredBytes, (totalNotRequiredBytes*100.0)/(totalFoundBytes+totalNotRequiredBytes));

    ptr->allocated = 1;
    
    /* Reduce internal fragmentation: Is this necessary? */
    if (ptr->len != requestedLen) {
        
        /* Create new node for the unused space: */
        TMemoryNode* unusedSpace = newNode(ptr->startAddress + requestedLen, ptr->len - requestedLen, 0);
        
        ptr->len = requestedLen;
        insertNode(unusedSpace);

        /* Merge with trailing space (if any) */
        if (testAdjacent(unusedSpace)) {
            mergeNodes(unusedSpace);
        }
    }


    _allocated_length = requestedLen;

    return (void *) ptr->startAddress;
}

// Pre: pointerToFree = Starting address of memroy segment to free
// Post: Memory segment is freed

void NSFree(void *pointerToFree)
{
    // Implement the free memory routine.
    // Locate the node corresponding to the memory to free, set the allocated flag
    // to false to "deallocate" the memory, and do a merge if possible with neighboring
    // "free memory" nodes.
    // ENSURE THAT YOUR CODE IS PROPERLY DOCUMENTED!!


    TMemoryNode *ptr = _head;

    /* Navigate to the starting address: */
    while (ptr != NULL) {
        if (ptr->startAddress == (unsigned long) pointerToFree) {
            break;
        }
        ptr = ptr->next;
    }

    if (ptr != NULL) {
        /* 'deallocate' memory space */
        ptr->allocated = 0;

        /* Merge trailing space  */
        if (testAdjacent(ptr)) {
            mergeNodes(ptr);
        }

        /* Merge leading space */
        if (testAdjacent(ptr->prev)) {
            mergeNodes(ptr->prev);
        }

    } else {
        printf("Could not find the address of memory to be freed\n");
    }
}



