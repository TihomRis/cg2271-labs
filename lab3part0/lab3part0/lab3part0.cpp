/*
 * lab3part0.cpp
 *
 * Created: 2/8/2014 
 *  Author: MohitSridhar
 */ 


#include <avr/io.h>
#include <Arduino.h>
#include <stdlib.h>
#include <limits.h>

int fun1 (int x, int y)
{
	return x+y;
}

int fun2 (int x, int y)
{
	return x*y;	
}

void setup()
{
	Serial.begin(9600);
}

int (*funcptr) (int, int);

void loop()
{
	float turn = (float)  rand() / INT_MAX;
	int result;
	
	if (turn > 0.5)
		funcptr=fun1;
	else
		funcptr=fun2;
	
	result = funcptr(2,3);
	
	Serial.print("Computation result: ");
	Serial.println(result);
}

int main(void)
{
    init();
	setup();
	
	while(1)
    {
         loop(); 
		 if(serialEventRun)
			serialEventRun();
    }
}