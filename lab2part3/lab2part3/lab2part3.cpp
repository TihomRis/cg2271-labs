/*
 * lab2part3.cpp
 * Author: Mohit Shridhar
 */ 


#include <avr/io.h>
#include <Arduino.h>

#define INPUT1INT 0
#define INPUT2INT 1

#define INPUT1 A0
#define INPUT2 A1

#define analogOut 6
#define digitalOut 7

unsigned char data1Flag = 0;
unsigned int data1;

unsigned char data2Flag = 0;
unsigned int data2;

long prevMillis = 0;
int ledState = LOW;

/**
	Note: The following flashPin function doesn't use the 'delay' function. While using the delay function
	either "blinking" or "brightness adjustment" can be executed with the required priority. Simultaneously controlling 
	both is not possible since Arduino doesn't have a multi-core processor. So the work around is to use the inbuilt timer
	function to process delays and continue looping through the rest of the program instead of suspending the entire
	program while processing the flashing part like in delay function. 
*/

void flashPin(int pin, int delayVal)
{
	  long currMillis = millis();
	  
	  if(currMillis - prevMillis > delayVal && !data1Flag) {
		  // save the last time you blinked the LED
		  prevMillis = currMillis;

		  // if the LED is off turn it on and vice-versa:
		  if (ledState == LOW)
		  ledState = HIGH;
		  else
		  ledState = LOW;

		  // set the LED with the ledState of the variable:
		  digitalWrite(pin, ledState);
	  }
}

void data1ISR()
{
	data1 = analogRead(INPUT1);
	data1Flag = 1;	
}

void data2ISR()
{
	data2 = analogRead(INPUT2);
	data2Flag = 1;
	
}

int remap1023to255(int val)
{
	double percentage = val / 1023.0;
	return percentage * 255.0; // Auto-unboxing: INT conversion
}

int remapto255 (int val, int inLow, int inHigh)
{
	double percentage = (val * 1.0) / (inHigh - inLow);
	return (percentage * 255.0) + inLow; // Auto-unboxing: INT conversion
}

void setup()
{
	attachInterrupt(INPUT1INT, data1ISR, RISING);
	attachInterrupt(INPUT2INT, data2ISR, RISING); //Use HIGH on Arduino DUE: helps while pressing down the button
	
	pinMode(analogOut, OUTPUT);
	pinMode(digitalOut, OUTPUT);
	Serial.begin(9600);
}

void processSensor1 (int val)
{
		val = remap1023to255(val);
		Serial.print("Potentiometer: ");
		Serial.println(val);
		
		analogWrite(analogOut, val);	
}

void processSensor2 (int touch)
{
		touch = remapto255(touch, 0, 1023);
		Serial.print("Touch Sensor: ");
		Serial.println(touch);
			
		flashPin(digitalOut, touch);
}

void loop()
{
	if (data1Flag) 
	{
		processSensor1(data1);
		data1Flag = 0;
	}
	
	if (data2Flag)
	{
		processSensor2(data2);
		data2Flag = 0;		
	}
	
	if (!data1Flag)
		flashPin(7, data2);	
}

int main(void)
{
	init();
	setup();
    while(1)
    {
		loop();
		
		if(serialEventRun)
			serialEventRun();
    }
	return 0;
}